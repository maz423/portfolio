import './Projects.css'
import React, { Component } from 'react';
import Form from 'react-bootstrap/Form'
import Button from 'react-bootstrap/Button'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import { Link } from 'react-router-dom';
import { Nav } from 'react-bootstrap';
import { Navbar } from 'react-bootstrap';
import { NavDropdown } from 'react-bootstrap';
import { Container } from 'react-bootstrap';
import { Modal } from 'react-bootstrap';
import { useState } from 'react';
import {motion} from 'framer-motion'
import Carousel from 'react-bootstrap/Carousel'




export const Projects = (props) => {


        

return (
<div className='page'>   




{/* ------------------------------------------------------ */}

<motion.div
initial={{x:-1000}} animate={{x: 0 }} transition={{delay:0.4}}
className='skill-div'
>
   
<Row>

<Col>
<h1 className='skill-head' >Ecommerce-app :</h1>   



<p className='skills-project'> The web app allows University students to buy or sell used books and allows book stores to list their books. <br></br>This was a Team-based project where I was responsible for designing and implementing the front end of the web application using React, Bootstrap, HTML, CSS, JavaScript.<br></br>This app also uses Mongo DB and Nodejs.<br></br><br></br><br></br> <p className='skill-head'>Team members :</p>Brett Popowycz (bwp265) - bwp265@usask.ca<br></br>John Gamueda (jdg315) - jdg315@usask.ca<br></br>Mandy Fraser (mmf590) - mmf590@usask.ca<br></br>Ron Sungcang (rts966) - rts966@usask.ca</p>



<motion.p animate={{x:0}} className='skill-head' >Github link : <li ><a href='https://gitlab.com/maz423/bookshop'><motion.img whileHover={{scale : 1.3}} className='Git' src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAIMAAACDCAMAAACZQ1hUAAAAY1BMVEUAAAD///+jo6PPz8/V1dUZGRlERETq6urj4+NmZmb8/PxYWFiRkZFLS0v09PS7u7sKCgp3d3dvb28uLi6BgYEnJyfCwsKwsLBSUlIzMzOpqalfX1/d3d06OjqLi4uZmZkgICA24B/YAAAFb0lEQVR4nO1c65ayOgwFAcu1ihcUwYH3f8pDdRigSekNhbPWt//MWs4Etm2S7ibtOG4PfxcFzrcQRDv/783O78/D/mvv77E/TDiQ6OsMGCIycPCOq1BwnKPXc/BWYsDgvTmQtUaB4UheHNbxhR4R43BYlYLjHDoO3w/KKfau469MwekY7Nam0DFY1yMZIud7a4QI6zP4hyUQXM/3/Ichv5+vwdcz/j29+cQdg/i3NPna+5PUd0Xwmy/w2Dd1LGTAEIfNZ7N/UswT+KVRfG4wLqECgTfCy0cY5OoMGEi+OINM7Ici+PdlKey0GTAsuSZXRP4+FKRaikKjEgw44mYRBsFB/qoZHBZYmku9cIAIS1sKmfk89IgzOwqVPYWOhJVnJktQ6EhY5O5kEQYMxiTKZUaBITZ0zKttRIwRXo04eAtSMCww3Bal4Lo3fQrR9AmnKN/VVPmFtN7l0Wn6mfaO7jh933tn/EzVWND0+fr76XpPdZU3t0j89J+f5DrCP/V/nE5/oVnp4IYxHsmRfFjIKSFhWIchIcP4kJGK41PcCb5oBlxY0vHvrsxbqdfkUVa270/KLMobjxG5TWKQm7pQhwI3iLxxlZ5Rs3PKrQx8hkk1OPAJ0rR6yKeYWN204UzdwpBDwT9IWVa1QD4uxoG0ipY/vOViczEKcgngWkUMOUA9rhgaJ2DoUjNhGiBpVS1HYDrabOOWI09SS5aYcnkYcXggT1IKT4w8MVMgV2x/pjKkiG4w1qSYJlbQEQHCXTWgIGCYu0Tu3yW00lpqOCCaVC5vL9DIpqZi9DiQXqcLtzZgipAn/hrYmMVlDxiftcykhC5p112AWZfIHAJGk2Fu6HEGX0oa6RFvIR86CeDkygYW+rGpdugBnVwWGLyStHVJLO/KVCV0Y9sCHywryr4VHDnbyhrkIJtdqL5s5wKOrD4Hg/3yBNAfZOoUcrDth0NZJuMA/cG2GQ03ybK5gKxtVm4GuHrLZhd68eK5WhrtUPjYVTixKqtMliGK1q4thKg5maqtoIndggGd3JUNbAZ1j8aGHQGcCiqrobeICLXpjiFzG7YyI6QyanN+yuhx2P7MvHG7R54mX4CwWr15qsQqifJNW4tYGW+0kMB03VZuh5XKqdlsIEGm5l2gIMZgVIg5om1RFU2EbDg71PpNkBIqagalB+Gtk1C3e50InqNkjE6Gq+uYqDsqTkU3hqIGQahe8z6J+lBUcU7/dAxNk9Nh/LQ6V3nEc+aohKoyvE8MsnS87JAin9c050sx12hR9qq/FJGyY7dVwH2tsNjlVQusrqfLo5CcUlBferLBhjUJ0j2MMix9KxwV0Wh9D7mSPrtn/+x5FYDWEJ7Soxo6K/AwEC59tUv47Tiu70TxaDIMkxU8PDpXkkzHWVDfw+rTY+htG8d95s7ylk71lSjC5o+M6PabR2Wk+DUb2ZiESGrPT4ZeW2/6ldi+qAivwycEb6nhskk6eDMYnDzuZj8r9k7QeHVYe48ZMTFDwWT5vw8O9l5nEtI4bbJ3Sk+87ogpUKNTY0M80lf7Ong0ndokTjVzIE/MwXC3NtTHhnbcc75xLqRgXFEaSijkqWYhomBRzBk2izRVaqsJKFhtWUcRSh99fbUUb/4+QIHfdsVhHbJw0eNgW9lD9356HKwp4PlXh4N5M2yEDMoCdQ7E8sBejyOopShzKJa72HChRhzooiec28KAQ2FXVYSYKCkVDv4nDpuPdi5yDuHyR7x/WfiKHPxPMWDIbsw7xcVeVmqmt4XiUYigmr3qkaTVv3tG/2esP3vBJu7rbeHe4hbub27hHusm7vNu4V7zJu53b+Ge+ybu+2/i/x64W/j/Dy/ts9r/wfgPlr45bzQOO30AAAAASUVORK5CYII=' height={50} width={50} /></a></li>
</motion.p>
</Col>

<Col>
<Carousel className='carousel'>
      <Carousel.Item>
      <motion.img className='imgs' initial={{opacity:0}} animate={{opacity:1}}    src='https://drive.google.com/uc?export=view&id=1meZF5I-NWwO_NbLz3BStYZUoHpt9eOQy' height={400} width={700} />

        <Carousel.Caption>
          <div className='corousel-div'>
          <h3  className='font-Roman'> Homepage</h3>
          <p>Click arrow to see more</p>
          </div>
        </Carousel.Caption>
      </Carousel.Item>
      <Carousel.Item>
      <motion.img className='imgs' initial={{opacity:0}} animate={{opacity:1}} transition={{delay:1.5}}   src='https://drive.google.com/uc?export=view&id=1YovSFusdHr7JbCrUsssZGhx7cXBGCxIF' height={400} width={700} />


        <Carousel.Caption>
        <div className='corousel-div'>
          <h3 className='font-Roman' >Buy Books
          </h3>
          <p>Users have the option of making an offer or adding a book to their wishlist</p>
          </div>
          
        </Carousel.Caption>
      </Carousel.Item>
      <Carousel.Item>
      <motion.img className='imgs' initial={{opacity:0}} animate={{opacity:1}} transition={{delay:1.5}}   src='https://drive.google.com/uc?export=view&id=14KTb_8P3_-6OmBRZbYoPBLlTH0PMUdyE' height={400} width={700} />


        <Carousel.Caption>
        <div className='corousel-div'>
          <h3 className='font-Roman' >Send offers to buyers</h3>
        </div>
          
        </Carousel.Caption>
      </Carousel.Item>
    </Carousel>

</Col>


</Row>
</motion.div>

{/* ------------------------------------------------------ */}

<motion.div
initial={{x:-1000}} animate={{x: 0 }} transition={{delay:0.2}}
className='skill-div'
>
   
<Row>
<Col>
<h1 className='skill-head' >Portfolio web app :</h1>   


<p className='skills-project'> React web-app displaying my skills and projects i have done so far. <br></br>This web-app uses Framer-motion, bootstrap, HTML, CSS, JavaScript. This app is still a work in progress and has been optimized for mobile browsers as well.<br></br> </p>



<motion.p animate={{x:0}} className='skill-head' >Github link : <li ><a href='https://gitlab.com/maz423/portfolio'><motion.img whileHover={{scale : 1.3}} className='Git' src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAIMAAACDCAMAAACZQ1hUAAAAY1BMVEUAAAD///+jo6PPz8/V1dUZGRlERETq6urj4+NmZmb8/PxYWFiRkZFLS0v09PS7u7sKCgp3d3dvb28uLi6BgYEnJyfCwsKwsLBSUlIzMzOpqalfX1/d3d06OjqLi4uZmZkgICA24B/YAAAFb0lEQVR4nO1c65ayOgwFAcu1ihcUwYH3f8pDdRigSekNhbPWt//MWs4Etm2S7ibtOG4PfxcFzrcQRDv/783O78/D/mvv77E/TDiQ6OsMGCIycPCOq1BwnKPXc/BWYsDgvTmQtUaB4UheHNbxhR4R43BYlYLjHDoO3w/KKfau469MwekY7Nam0DFY1yMZIud7a4QI6zP4hyUQXM/3/Ichv5+vwdcz/j29+cQdg/i3NPna+5PUd0Xwmy/w2Dd1LGTAEIfNZ7N/UswT+KVRfG4wLqECgTfCy0cY5OoMGEi+OINM7Ici+PdlKey0GTAsuSZXRP4+FKRaikKjEgw44mYRBsFB/qoZHBZYmku9cIAIS1sKmfk89IgzOwqVPYWOhJVnJktQ6EhY5O5kEQYMxiTKZUaBITZ0zKttRIwRXo04eAtSMCww3Bal4Lo3fQrR9AmnKN/VVPmFtN7l0Wn6mfaO7jh933tn/EzVWND0+fr76XpPdZU3t0j89J+f5DrCP/V/nE5/oVnp4IYxHsmRfFjIKSFhWIchIcP4kJGK41PcCb5oBlxY0vHvrsxbqdfkUVa270/KLMobjxG5TWKQm7pQhwI3iLxxlZ5Rs3PKrQx8hkk1OPAJ0rR6yKeYWN204UzdwpBDwT9IWVa1QD4uxoG0ipY/vOViczEKcgngWkUMOUA9rhgaJ2DoUjNhGiBpVS1HYDrabOOWI09SS5aYcnkYcXggT1IKT4w8MVMgV2x/pjKkiG4w1qSYJlbQEQHCXTWgIGCYu0Tu3yW00lpqOCCaVC5vL9DIpqZi9DiQXqcLtzZgipAn/hrYmMVlDxiftcykhC5p112AWZfIHAJGk2Fu6HEGX0oa6RFvIR86CeDkygYW+rGpdugBnVwWGLyStHVJLO/KVCV0Y9sCHywryr4VHDnbyhrkIJtdqL5s5wKOrD4Hg/3yBNAfZOoUcrDth0NZJuMA/cG2GQ03ybK5gKxtVm4GuHrLZhd68eK5WhrtUPjYVTixKqtMliGK1q4thKg5maqtoIndggGd3JUNbAZ1j8aGHQGcCiqrobeICLXpjiFzG7YyI6QyanN+yuhx2P7MvHG7R54mX4CwWr15qsQqifJNW4tYGW+0kMB03VZuh5XKqdlsIEGm5l2gIMZgVIg5om1RFU2EbDg71PpNkBIqagalB+Gtk1C3e50InqNkjE6Gq+uYqDsqTkU3hqIGQahe8z6J+lBUcU7/dAxNk9Nh/LQ6V3nEc+aohKoyvE8MsnS87JAin9c050sx12hR9qq/FJGyY7dVwH2tsNjlVQusrqfLo5CcUlBferLBhjUJ0j2MMix9KxwV0Wh9D7mSPrtn/+x5FYDWEJ7Soxo6K/AwEC59tUv47Tiu70TxaDIMkxU8PDpXkkzHWVDfw+rTY+htG8d95s7ylk71lSjC5o+M6PabR2Wk+DUb2ZiESGrPT4ZeW2/6ldi+qAivwycEb6nhskk6eDMYnDzuZj8r9k7QeHVYe48ZMTFDwWT5vw8O9l5nEtI4bbJ3Sk+87ogpUKNTY0M80lf7Ong0ndokTjVzIE/MwXC3NtTHhnbcc75xLqRgXFEaSijkqWYhomBRzBk2izRVaqsJKFhtWUcRSh99fbUUb/4+QIHfdsVhHbJw0eNgW9lD9356HKwp4PlXh4N5M2yEDMoCdQ7E8sBejyOopShzKJa72HChRhzooiec28KAQ2FXVYSYKCkVDv4nDpuPdi5yDuHyR7x/WfiKHPxPMWDIbsw7xcVeVmqmt4XiUYigmr3qkaTVv3tG/2esP3vBJu7rbeHe4hbub27hHusm7vNu4V7zJu53b+Ge+ybu+2/i/x64W/j/Dy/ts9r/wfgPlr45bzQOO30AAAAASUVORK5CYII=' height={50} width={50} /></a></li>
</motion.p>


<motion.p animate={{x:0}} className='skill-head' >Website link : <li ><a href='https://mohammadzargar.netlify.app/'><motion.img whileHover={{scale : 1.3}} className='Git' src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAIkAAACJCAMAAAAv+uv7AAAAeFBMVEX/////pQD/pAD/ogD/oAD/8dn//fr/u1L/qRv/7dD/0pP/rjD/xWn/pAn/+uz///3/wWD/9eX/sD//vlj/6sr/+vD/0Y7/xnP/9OD/26X/zYX/sTf/tkb//fb/4rT/y33/5bz/2p7/1Jz/4r//2a3/tTT/mQD/ryfsuDgrAAALAklEQVR4nO0c2YKqOkzasgmyCILKPs4c/v8PL9AtRazIzHhfJm+UNKRpmiZpym63FcKTmd4+s9yP/Ly57j9vt1tqVuFmept4qMzbR1QeHUwIwROQETByjnbzmcbWe/gw68Z2pu8iYw4IDTyhwu/S6pe5qNysNAYx3POg8IMJ9qI2/j0+gq40nnEhucFe7v7KNFlpPkhjHRuMGULKLvhpPk4Xe600IGDiXM2fXE7VyMfLbFBe8MDLjzFSe2QjHyMgYuTnn2AjdJ/IA00GRTdziDj707cZCTJDx8ewXp1jaZe9XpsRsdNvCqTtieYDBNv7ND5ZYRWYdd7r5hDj6DvLKGh0AsGGD01pGHeeRi4IF+1mRsxCIxBEyjuBB3udSmGj2bagw05L1rksdTpH2tkstizoU6NTQVy4D7pl2m5e+zIjsX5w9mML0Wl1Cy+KUgNuTxQKKlck0pmHVsVWuyJ8fUlZXEcdF+6Vp1K/x9bKBDmqjBDOX/Bd2l7pjHHTgQZcPvM8OgSx60KVL8lXG9xWlQjx6iCRLah/ugDCDHybtPFM+bG/kpVUYWSwG+buAhhZo3MnW3bA5Wl3UWdo5QSlSi9sZOEuBrOFmzVEYjAakg12plSWIslXeHNnRUfIZKKvgBF7nWQ/5YeRY47mSXHAyfPxBLbKyGg3AkkE4QcWbQ5hLlUFX4eGag+l8nyOKx/qOaKcN4Bot46RgVIv2SfTNtz2Ciu1vv9VtSOlP4IDW5IkiSbwH8H0NknAdzGl8wXnB3vaJdjOTDyN7RQCtOk5KAuXxYgq7UJjbE3ndfd9O2i0toq+4ThvANw+4mT/XkaG+XlgEUzjnXMzseIvz41Pnvf9WUBk0eO/vJ2RBxb71L9ZSyYg+3tO4EaOBMBeaAss95dtzp2vc4bq6gg4LLauhwf9ZeudUQnB3oKy8wimabqmlBQuzQ1QiylHxkBugJG0W8Dm2QqWuwT2gBbJfWjaUF+Gkxw+AfMAPC8yowv38FY2Vx4nhA7bQmxffJMAP8CSvgdyFD/jDFiPgDvlSv7KbQH2RcraBzteKtVHFfaHFAmBLEobg/NNjIBNFfUwnJdmFDtgjJa0JQR+Mcxl+xPH5hFUYB7g/J6l1wCnrRUMIgeiByUng4yt2VW5KPEHbJeDxLbUB+kNEDiZQyiIBPbWpHMqOCH5Mm2DiIUM5tJQVohUtyWrvA5OwmSqSg9nXmjEnh0DDJ6huiM14sW/disnlc2JkINixFosXvR86us9h0w1Gp/yxfYTgVsmqCicVBfRvv/FFP8f/MEf/MH/CVWdZfuuBuavuuRNVoM9ouryoSWd9i+r3WdZVoPE8Km9dF0NrGRcd92lpocL1YiuvD3V1+Z6UdCzZvhetWv/0SyDoB3m0/OX2KqtZGr4N3koe4oudopdVU4NhdgOgq8J5d/kinUUXfoUIUMX+5tVUOq3XUq3yjEtR6HFM9eIub80cmQPWOzNNe2PP2foNH3FPB8sqZOZd1QzjHR3Ok6ksMcHxZws4ZGEtIEhBNSDEGmYMOHoloKOqE/IIkt8rLh8GXqxU9G9Af1KwJAHz4a7DbzBpQ4Gd3xZFMDdXZd7gRzdZO9ZRNXQD3HPUfhNeIZ+lV9i7oolwgKSUFzK6ZTKnD6NoANmifCE+BZAN3hAxVwz7pAnghOGTvvT+Kvi8pm0Cvhv9NunI5UvT3VUQJxL6AUdF49VnqDHNKDC0TR7LJRAkxKBQJ1mo7mK3biKMXSq0SCIpfNRq3Mt0dtJAmrmWvin9K1QwmjkUUasA9snEZjgUq5SpjeJ7CrRmb5DdIoyueoQHRfjHFAFxgWTIFWNKZCgSxDJhczkCWOSnC2OsxgwQ7+IDBlMIFOxIW+OPqaGzzSBIWyCaXBKIV10DhvZ4F/vmUaBEDVlMvzgYfOhNPjIMrbigYXmq+ODqRwyGiYIrgtIoDObgKOQzjL2eTYl3TnKEqZKyNBtZgRxYlI1xTfLu0ff0fAN97ua6X7AtNo9UV0A6YKaLyUqi0F/6OIiTcqWsBLPsyk0TIZVs6WrRR8+zFSu5ehXvhrkLlYxS8iGNqgbm+9Dz2yNktgO+NKbBDYMls0ActgYZ+iULHIOlHrIqTs9QwfIGQaqNC1nFqCyTWWWO2HWi6obGXccZjXYmnyATmkNiho+pg6TwjRz3MIjyHlQfAbZQYp+g+hfKja34vTlFJK2BqSunH8Bk0NDeuUk7y5jAV7S9W3p0K1ojl6VgHP1zKmWx0LM9ZBJHOTchaIgkcfQP2Vy4j4bD9BZ1rMDmUQ1WSVFwB0VmasjH3PKYEwCXRhncn+uVonzSb6+A+8hdS4CZPAR8bQj2xpn6Pylw1fgnhMoFtDFy/4O3Znn72KuyCJTy/dN6c0BOLMxyfTbmY2bZAtHWTyjKPPAMZ63CGjGQkaEHela+zS/YSwmHTOCJnRpw3LaYixWYTTsZQxapu+he3TTPo4AZs0sJ1g+To0pOkg1mcnYUC4nn0xvegloxRR9SeDhaQBllYRWVVUPD7tH9NNdy6OMz4RtrUb/gz/4gz/4JsSm6TJQTZVodtPtJcixIOIq+0YoqZvcwDXowGBWP5QT8WJlKc49hDlmNNQTm7Fmjr0wRDVYKk59sFohthcvyOpanDnIE3Gs+gsZOODhbcCXU12AVJ7v5Funx5TemQ93mdhbOk8DvpziFsXyaLTfWggtaatnRHvpnQLagXS+lZIoUBqzWVHAKVsLScuCN+XEWCYfkOIYScYXPbcVAMTqLFNWT9FTEAbAT8qoByfbXImbIKxETZLBIZSE6zUEZ6nwjOwEckDbqvcbOQlQTWAZQ6t0AEHZlNHhHMrDePRqse8ElcwMwRg1BYoZqcIGuomMNrQYhB9gDe6slyE0hUVChqQKi8TuCrhaIK7eH2v0ksS27ULGhgd7A8Coc3gcqSaRn4AZKO8MFazFxQLU8r7X4UF/0HhfwgED+vcBWaojA+r8NlhekTAB+i5YCP5HqN8uFFw8MJf+m4Uyq6cAcH7z/ODHe1n9Vk7G8veHkM1U5TuFfgu9VEa0ZT5KJm4AbwLY4jhO3/feMxhwDoARZ2wrlIpkZOg3srOnsJKdggGg+e2m+r94hGAZpnfn+Cw3CoO0A52TqQxToyQUUsg4ws3oO7hwT1pb1gaKXKcDmJ2pDJL4T/0d9coLyeUpDG1YdRliLF+UmzsaT6la5cbKqkK9vaK1ZLxGlcI60lWFbTBTTKJwDFgUifSr/K79bAf9sBQXa168uQTw2sx4snFO1KL9dYzsrKtaYIvz4CzdzmHWnwcc8IIVycLWU+SM+9WBwlU1K+SrBWWkK668wMtZyDOv6nW+FxjZWZ16KwI7ifL4RGsDeBUJlcfZjSbnpUuBqq7wI0ohFW2UHMzM4+zeXPFikFA7uj0IZ48nyCw03gUi/st1tq5uY0bo4f21VNvPaDYEcGdbO7ZkWcgXnSwx6TalHELtzVhi7O+HZ5a627d40wXWCVqtpIldq2n+c6MViJF/o9AybrT3qEnZ8RSZFbe5ob306tTf+11Aq5P3eA29iPLrtclLh2gRUfPtytMgO+jWM1qK9u6QSNH+xI8lTP+1v0jcyYMYl5/6wYXrH3Si1/OBjx/f/1mBgCqN0Ca5YNJnP12abF6NV/8pgQg51j8oDwHBxV79+5Nf/QHKAJabHdf8BGX8R40T1b9cMe5eogKTx+xMXJTNb/6bRoAVpx/R0UHix0EChpaDZzf1O39mFFrn9PbZRArk+8+bG2xl4j/x77Cr37IxbwAAAABJRU5ErkJggg==' height={50} width={50} /></a></li>
</motion.p>
</Col>

<Col>
<Carousel className='carousel'>
      <Carousel.Item>
      <motion.img className='imgs' initial={{opacity:0}} animate={{opacity:1}}    src='https://drive.google.com/uc?export=view&id=1KP5qux4yDVNEV-O5ZYM-InHnrAdFEyxT' height={400} width={700} />

        <Carousel.Caption>
          <div className='corousel-div'>
          <h3  className='font-Roman'> Homepage</h3>
          <p>Click arrow to see more</p>
          </div>
        </Carousel.Caption>
      </Carousel.Item>
      <Carousel.Item>
      <motion.img className='imgs' initial={{opacity:0}} animate={{opacity:1}} transition={{delay:1.5}}   src='https://drive.google.com/uc?export=view&id=1hSJ6AZEysuQH3DRa_Rna7OjKnKzcnyfp' height={400} width={700} />


        <Carousel.Caption>
        <div className='corousel-div'>
          <h3 className='font-Roman' >My Skills
          </h3>
          <p>List of some technologies i have experience working with</p>
          </div>
          
        </Carousel.Caption>
      </Carousel.Item>
      
    </Carousel>

</Col>



</Row>
</motion.div>

{/* ------------------------------------------------------ */}

<motion.div
className='skill-div'
initial={{x:-1000}} animate={{x: 0 }} transition={{delay:0.6}}
>
   
<Row>
<Col>
<h1 className='skill-head' > Online registeration system for a CBO :</h1>   


<p className='skills-project'> Designed a 3 tier Web application to help the organization in keeping track of their staff members and manage the records of their customers.<br></br> Using React, JavaScript, HTML, MySQL, and Docker (Full-stack web development). </p>



<motion.p animate={{x:0}} className='skill-head' >Github link : <li ><a href='https://gitlab.com/maz423/cbo'><motion.img whileHover={{scale : 1.3}} className='Git' src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAIMAAACDCAMAAACZQ1hUAAAAY1BMVEUAAAD///+jo6PPz8/V1dUZGRlERETq6urj4+NmZmb8/PxYWFiRkZFLS0v09PS7u7sKCgp3d3dvb28uLi6BgYEnJyfCwsKwsLBSUlIzMzOpqalfX1/d3d06OjqLi4uZmZkgICA24B/YAAAFb0lEQVR4nO1c65ayOgwFAcu1ihcUwYH3f8pDdRigSekNhbPWt//MWs4Etm2S7ibtOG4PfxcFzrcQRDv/783O78/D/mvv77E/TDiQ6OsMGCIycPCOq1BwnKPXc/BWYsDgvTmQtUaB4UheHNbxhR4R43BYlYLjHDoO3w/KKfau469MwekY7Nam0DFY1yMZIud7a4QI6zP4hyUQXM/3/Ichv5+vwdcz/j29+cQdg/i3NPna+5PUd0Xwmy/w2Dd1LGTAEIfNZ7N/UswT+KVRfG4wLqECgTfCy0cY5OoMGEi+OINM7Ici+PdlKey0GTAsuSZXRP4+FKRaikKjEgw44mYRBsFB/qoZHBZYmku9cIAIS1sKmfk89IgzOwqVPYWOhJVnJktQ6EhY5O5kEQYMxiTKZUaBITZ0zKttRIwRXo04eAtSMCww3Bal4Lo3fQrR9AmnKN/VVPmFtN7l0Wn6mfaO7jh933tn/EzVWND0+fr76XpPdZU3t0j89J+f5DrCP/V/nE5/oVnp4IYxHsmRfFjIKSFhWIchIcP4kJGK41PcCb5oBlxY0vHvrsxbqdfkUVa270/KLMobjxG5TWKQm7pQhwI3iLxxlZ5Rs3PKrQx8hkk1OPAJ0rR6yKeYWN204UzdwpBDwT9IWVa1QD4uxoG0ipY/vOViczEKcgngWkUMOUA9rhgaJ2DoUjNhGiBpVS1HYDrabOOWI09SS5aYcnkYcXggT1IKT4w8MVMgV2x/pjKkiG4w1qSYJlbQEQHCXTWgIGCYu0Tu3yW00lpqOCCaVC5vL9DIpqZi9DiQXqcLtzZgipAn/hrYmMVlDxiftcykhC5p112AWZfIHAJGk2Fu6HEGX0oa6RFvIR86CeDkygYW+rGpdugBnVwWGLyStHVJLO/KVCV0Y9sCHywryr4VHDnbyhrkIJtdqL5s5wKOrD4Hg/3yBNAfZOoUcrDth0NZJuMA/cG2GQ03ybK5gKxtVm4GuHrLZhd68eK5WhrtUPjYVTixKqtMliGK1q4thKg5maqtoIndggGd3JUNbAZ1j8aGHQGcCiqrobeICLXpjiFzG7YyI6QyanN+yuhx2P7MvHG7R54mX4CwWr15qsQqifJNW4tYGW+0kMB03VZuh5XKqdlsIEGm5l2gIMZgVIg5om1RFU2EbDg71PpNkBIqagalB+Gtk1C3e50InqNkjE6Gq+uYqDsqTkU3hqIGQahe8z6J+lBUcU7/dAxNk9Nh/LQ6V3nEc+aohKoyvE8MsnS87JAin9c050sx12hR9qq/FJGyY7dVwH2tsNjlVQusrqfLo5CcUlBferLBhjUJ0j2MMix9KxwV0Wh9D7mSPrtn/+x5FYDWEJ7Soxo6K/AwEC59tUv47Tiu70TxaDIMkxU8PDpXkkzHWVDfw+rTY+htG8d95s7ylk71lSjC5o+M6PabR2Wk+DUb2ZiESGrPT4ZeW2/6ldi+qAivwycEb6nhskk6eDMYnDzuZj8r9k7QeHVYe48ZMTFDwWT5vw8O9l5nEtI4bbJ3Sk+87ogpUKNTY0M80lf7Ong0ndokTjVzIE/MwXC3NtTHhnbcc75xLqRgXFEaSijkqWYhomBRzBk2izRVaqsJKFhtWUcRSh99fbUUb/4+QIHfdsVhHbJw0eNgW9lD9356HKwp4PlXh4N5M2yEDMoCdQ7E8sBejyOopShzKJa72HChRhzooiec28KAQ2FXVYSYKCkVDv4nDpuPdi5yDuHyR7x/WfiKHPxPMWDIbsw7xcVeVmqmt4XiUYigmr3qkaTVv3tG/2esP3vBJu7rbeHe4hbub27hHusm7vNu4V7zJu53b+Ge+ybu+2/i/x64W/j/Dy/ts9r/wfgPlr45bzQOO30AAAAASUVORK5CYII=' height={50} width={50} /></a></li>
</motion.p>
</Col>





<Col>
<Carousel className='carousel'>
      <Carousel.Item>
      <motion.img className='imgs' initial={{opacity:0}} animate={{opacity:1}}    src='https://drive.google.com/uc?export=view&id=1b2CdmY_luX6SpWyg4mQ3VVQGF1eUUV1V' height={400} width={700} />

        <Carousel.Caption>
          <div className='corousel-div'>
          <h3  className='font-Roman'> Homepage</h3>
          <p>Click arrow to see more</p>
          </div>
        </Carousel.Caption>
      </Carousel.Item>
      <Carousel.Item>
      <motion.img className='imgs' initial={{opacity:0}} animate={{opacity:1}} transition={{delay:1.5}}   src='https://drive.google.com/uc?export=view&id=1hlqIjaORxwHUYfmA2N6s2qAGCPoc9vto' height={400} width={700} />


        <Carousel.Caption>
        <div className='corousel-div'>
          <h3 className='font-Roman' >Register staff members
          </h3>
          <p>Admins can register staff members or customers</p>
          </div>
          
        </Carousel.Caption>
      </Carousel.Item>
      <Carousel.Item>
      <motion.img className='imgs' initial={{opacity:0}} animate={{opacity:1}} transition={{delay:1.5}}   src='https://drive.google.com/uc?export=view&id=1FBF1Ali1HnbeF_HoZYymrA2cXVSLgvgy' height={400} width={700} />


        <Carousel.Caption>
        <div className='corousel-div'>
          <h3 className='font-Roman' >Displays staff members</h3>
        </div>
          
        </Carousel.Caption>
      </Carousel.Item>
    </Carousel>

</Col>
</Row>
</motion.div>

{/* ------------------------------------------------------ */}

{/* ------------------------------------------------------ */}


{/* ------------------------------------------------------ */}










</div>                  
                    
);


}