import './Skills.css'
import React, { Component } from 'react';
import Form from 'react-bootstrap/Form'
import Button from 'react-bootstrap/Button'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import { Link } from 'react-router-dom';
import { Nav } from 'react-bootstrap';
import { Navbar } from 'react-bootstrap';
import { NavDropdown } from 'react-bootstrap';
import { Container } from 'react-bootstrap';
import { Modal } from 'react-bootstrap';
import { useState } from 'react';
import {motion} from 'framer-motion'





export const Relevant = (props) => {


        

return (
<div className='page'>   


<motion.div
initial={{x:-1000}} animate={{x: 0 }} transition={{delay:0.2}}
className='skill-div'
>
   
<Row>
<Col>
<h1 className='skill-head' >
CMPT 141.3


 :</h1>   


<p className='skills'> Introduction to Computer Science</p>
</Col>







{/* ------------------------------------------------------ */}

<Col>

<h1 className='skill-head' >
CMPT 145.3


 :</h1>   


<p className='skills'> Principles of Computer Science</p>






</Col>
</Row>
</motion.div>

{/* ------------------------------------------------------ */}

{/* ------------------------------------------------------ */}

<motion.div
initial={{x:-1000}} animate={{x: 0 }} transition={{delay:0.2}}
className='skill-div'
>
   
<Row>
<Col>
<h1 className='skill-head' >
CMPT 214.3


 :</h1>   


<p className='skills'> Programming Principles and Practice</p>
</Col>







{/* ------------------------------------------------------ */}

<Col>

<h1 className='skill-head' >
CMPT 215.3


 :</h1>   


<p className='skills'> Introduction to Computer Organization and Architecture</p>






</Col>
</Row>
</motion.div>

{/* ------------------------------------------------------ */}

{/* ------------------------------------------------------ */}

<motion.div
initial={{x:-1000}} animate={{x: 0 }} transition={{delay:0.2}}
className='skill-div'
>
   
<Row>
<Col>
<h1 className='skill-head' >
CMPT 260.3


 :</h1>   


<p className='skills'> Mathematical Logic and Computing</p>
</Col>







{/* ------------------------------------------------------ */}

<Col>

<h1 className='skill-head' >
CMPT 270.3


 :</h1>   


<p className='skills'> Developing Object-Oriented Systems</p>






</Col>
</Row>
</motion.div>

{/* ------------------------------------------------------ */}


<motion.div
initial={{x:-1000}} animate={{x: 0 }} transition={{delay:0.2}}
className='skill-div'
>
   
<Row>
<Col>
<h1 className='skill-head' >
CMPT 280.3


 :</h1>   


<p className='skills'> Intermediate Data Structures and Algorithms</p>
</Col>







{/* ------------------------------------------------------ */}

<Col>

<h1 className='skill-head' >
CMPT 332.3


 :</h1>   


<p className='skills'> Operating Systems Concepts</p>






</Col>
</Row>
</motion.div>

{/* _____________________________________________________ */}

<motion.div
initial={{x:-1000}} animate={{x: 0 }} transition={{delay:0.2}}
className='skill-div'
>
   
<Row>
<Col>
<h1 className='skill-head' >
CMPT 353.3


 :</h1>   


<p className='skills'> Full Stack Web Programming</p>
</Col>







{/* ------------------------------------------------------ */}

<Col>

<h1 className='skill-head' >
CMPT 360.3


 :</h1>   


<p className='skills'> Machines and Algorithms</p>






</Col>
</Row>
</motion.div>

{/* _________________________________________________ */}

<motion.div
initial={{x:-1000}} animate={{x: 0 }} transition={{delay:0.2}}
className='skill-div'
>
   
<Row>
<Col>
<h1 className='skill-head' >
CMPT 370.3


 :</h1>   


<p className='skills'> Intermediate Software Engineering</p>
</Col>







{/* ------------------------------------------------------ */}

<Col>

<h1 className='skill-head' >
CMPT 317.3


 :</h1>   


<p className='skills'> Introduction to Artificial Intelligence</p>






</Col>
</Row>
</motion.div>



</div>                  
);


}